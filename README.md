#### Assembling the app

Make sure you have an ANDROID_HOME defined or a local.properties file stating the Android SDK location.

To assemble the apk: 

```
./gradlew clean app:assembleDebug
```

```
adb install -r app/build/outputs/apk/debug/app-debug.apk
```

#### Unit Tests

I did the unit testing using both JUnit standard testing and the newest Spek framework for Kotlin.

In order to run the unit tests:

```
./gradlew app:testDebugUnitTest
```

#### Instrumentation Tests

I just did a simple instrumentation test (just to show that I could do it.) However, I'm much more of 
a fan of unit testing :) I do believe that a good architecture with good unit testing will cover a lot 
of ground.

In order to run the demonstration instrumentation test:

```
./gradlew app:assembleDebugAndroidTest
```

```
adb install -r app/build/outputs/apk/androidTest/debug/app-debug-androidTest.apk
```

```
adb shell am instrument -w -r   -e debug false -e class com.babylon.fabiocarballo.posts.PostsListActivityTest com.babylon.fabiocarballo.test/com.github.tmurakami.dexopener.DexOpenerAndroidJUnitRunner
```

But it is way more simpler to run in the IDE if you want to.



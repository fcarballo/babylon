package com.babylon.fabiocarballo.extensions

import android.net.Uri
import android.support.annotation.DrawableRes
import android.widget.ImageView
import com.squareup.picasso.Picasso
import java.io.File

fun ImageView.loadUrl(url: String?,
                      @DrawableRes placeholder: Int = -1) {

    val treatedUrl = url ?: ""

    val picasso = Picasso.with(context)

    val scheme = Uri.parse(treatedUrl)?.scheme?.toLowerCase()
    val isRawUrl = listOf("https", "http").contains(scheme)

    val requestCreator = if (isRawUrl) {
        picasso.load(treatedUrl)
    } else {
        picasso.load(File(treatedUrl))
    }

    if (placeholder > 0) {
        requestCreator
                .placeholder(placeholder)
                .error(placeholder)
    }

    requestCreator.into(this)
}

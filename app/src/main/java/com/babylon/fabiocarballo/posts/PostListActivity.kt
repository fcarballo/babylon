package com.babylon.fabiocarballo.posts

import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import com.babylon.fabiocarballo.R
import com.babylon.fabiocarballo.extensions.toGone
import com.babylon.fabiocarballo.extensions.toVisible
import com.babylon.fabiocarballo.internals.BaseActivity
import kotlinx.android.synthetic.main.activity_posts_list.*
import kotlinx.android.synthetic.main.layout_error.*
import javax.inject.Inject

class PostListActivity : BaseActivity(), PostListPresenter.View {

    @Inject
    lateinit var presenter: PostListPresenter

    private val postClickListener: BasicPostView.ClickListener =
            object : BasicPostView.ClickListener {
                override fun onPostClick(post: PostViewModel) {
                    presenter.onPostClick(post)
                }
            }

    private val postsAdapter by lazy { PostAdapter(this, postClickListener) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_posts_list)

        activityComponent.postListActivityComponentBuilder()
                .plus(PostListActivityModule(this))
                .build()
                .inject(this)

        with(recyclerView) {
            layoutManager = LinearLayoutManager(this@PostListActivity, LinearLayoutManager.VERTICAL, false)
            addItemDecoration(DividerItemDecoration(recyclerView.context, LinearLayoutManager.VERTICAL))
            adapter = postsAdapter
        }

        refreshLayout.setOnRefreshListener { presenter.start() }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }

    override fun setPosts(posts: List<PostViewModel>) {
        recyclerView.toVisible()
        errorView.toGone()

        postsAdapter
                .also { it.postsList = posts }
                .also { it.notifyDataSetChanged() }

        setLoadingVisibility(false)
    }

    override fun showEmptyPlaceholder() {
        displayError(R.string.error_empty)
    }

    override fun showNoConnectionPlaceholder() {
        displayError(R.string.error_connection)
    }

    override fun showTimeoutPlaceholder() {
        displayError(R.string.error_timeout)
    }

    override fun showUnknownProblemPlaceholder() {
        displayError(R.string.error_unknown)
    }

    private fun displayError(@StringRes errorMsgResId: Int) {
        errorView.toVisible()
        errorMessage.text = getString(errorMsgResId)

        recyclerView.toGone()

        setLoadingVisibility(false)
    }

    private fun setLoadingVisibility(visible: Boolean) {
        if (!visible) {
            refreshLayout.isRefreshing = false
        }

        if (visible) {
            loadingProgress.toVisible()
        } else {
            loadingProgress.toGone()
        }
    }
}
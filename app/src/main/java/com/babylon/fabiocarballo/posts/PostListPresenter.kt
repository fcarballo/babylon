package com.babylon.fabiocarballo.posts

import com.babylon.fabiocarballo.Navigator
import com.babylon.fabiocarballo.internals.Logger
import com.babylon.fabiocarballo.internals.exceptions.NoConnectionException
import com.babylon.fabiocarballo.internals.exceptions.TimeoutException
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription

class PostListPresenter(
        private val view: View,
        private val getPostListUseCase: GetPostListUseCase,
        private val postModelMapper: PostModelMapper,
        private val navigator: Navigator,
        private val logger: Logger) {

    private val subscriptions = CompositeSubscription()

    fun start() {
        subscriptions.add(
                getPostListUseCase.build()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    System.out.println("AQUI")
                                    logger.debug("received ${it.size} posts!")

                                    if (it.isEmpty()) {
                                        view.showEmptyPlaceholder()
                                    } else {
                                        view.setPosts(postModelMapper.toViewModel(it))
                                    }
                                },
                                {
                                    System.out.println("AQUI2")

                                    logger.error(it)

                                    when (it) {
                                        is NoConnectionException -> view.showNoConnectionPlaceholder()
                                        is TimeoutException -> view.showTimeoutPlaceholder()
                                        else -> view.showUnknownProblemPlaceholder()
                                    }
                                }
                        )
        )
    }

    fun stop() {
        subscriptions.clear()
    }

    fun onPostClick(postViewModel: PostViewModel) {
        navigator.openPostDetails(postViewModel)
    }

    interface View {
        fun setPosts(posts: List<PostViewModel>)
        fun showEmptyPlaceholder()
        fun showNoConnectionPlaceholder()
        fun showTimeoutPlaceholder()
        fun showUnknownProblemPlaceholder()
    }
}
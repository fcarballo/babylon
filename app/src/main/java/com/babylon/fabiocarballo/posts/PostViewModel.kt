package com.babylon.fabiocarballo.posts

import org.parceler.Parcel

@Parcel
data class PostViewModel(
        val id: Long = -1,
        val username: String = "",
        val userAvatarUrl: String = "",
        val title: String = "",
        val body: String = "")

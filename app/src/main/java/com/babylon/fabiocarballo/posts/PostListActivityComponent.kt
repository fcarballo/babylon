package com.babylon.fabiocarballo.posts

import dagger.Subcomponent

@Subcomponent(modules = arrayOf(PostListActivityModule::class))
interface PostListActivityComponent {

    @Subcomponent.Builder
    interface Builder {
        fun plus(module: PostListActivityModule): Builder
        fun build(): PostListActivityComponent
    }

    fun inject(activity: PostListActivity)
}
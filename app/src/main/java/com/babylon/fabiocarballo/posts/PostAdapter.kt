package com.babylon.fabiocarballo.posts

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.babylon.fabiocarballo.R
import com.babylon.fabiocarballo.extensions.loadUrl
import kotlinx.android.synthetic.main.elem_post.view.*
import kotlinx.android.synthetic.main.elem_post_simple.view.*

class PostAdapter(private val context: Context,
                  private val clickListener: BasicPostView.ClickListener) : RecyclerView.Adapter<PostAdapter.Holder>() {

    var postsList: List<PostViewModel> = listOf()

    private val inflater by lazy { LayoutInflater.from(context) }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = inflater.inflate(R.layout.elem_post, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val view = holder.itemView
        val post = postsList[position]

        view.postView.let {
            it.bind(post)
            it.clickListener = clickListener
        }
    }

    override fun getItemCount() = postsList.size

    class Holder(view: View) : RecyclerView.ViewHolder(view)
}
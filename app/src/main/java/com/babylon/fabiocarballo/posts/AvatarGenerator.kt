package com.babylon.fabiocarballo.posts

class AvatarGenerator {

    fun generateAvatarUrl(email: String): String {
        return if (email.isBlank()) {
            ""
        } else {
            AVATAR_GENERATION_BASE_URL.replace(PARAM_AVATAR_GENERATION_EMAIL, email)
        }
    }

    companion object {
        private const val PARAM_AVATAR_GENERATION_EMAIL = "{email}"

        private const val AVATAR_GENERATION_BASE_URL = "https://api.adorable.io/avatars/48/$PARAM_AVATAR_GENERATION_EMAIL.png"
    }
}
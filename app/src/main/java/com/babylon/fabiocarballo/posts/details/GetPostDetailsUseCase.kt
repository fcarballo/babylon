package com.babylon.fabiocarballo.posts.details

import com.babylon.fabiocarballo.api.PlaceholderApi
import com.babylon.fabiocarballo.api.ServerPostFormat
import com.babylon.fabiocarballo.domain.Comment
import com.babylon.fabiocarballo.domain.DetailedPost
import com.babylon.fabiocarballo.posts.AvatarGenerator
import rx.Observable
import rx.Single

class GetPostDetailsUseCase(
        private val placeholderApi: PlaceholderApi,
        private val avatarGenerator: AvatarGenerator) {

    fun build(postId: Long): Single<DetailedPost> {
        return placeholderApi.getPost(postId)
                .flatMap { post ->
                    placeholderApi.getUser(post.userId)
                            .zipWith(getComments(post), { user, comments ->
                                DetailedPost(post.id, post.userId,
                                        user.username, avatarGenerator.generateAvatarUrl(user.email),
                                        post.title, post.body,
                                        comments)
                            })
                }
                .toSingle()
    }

    private fun getComments(post: ServerPostFormat) =
            placeholderApi.getComments(post.id)
                    .flatMap { Observable.from(it) }
                    .map { Comment(it.id, it.name, avatarGenerator.generateAvatarUrl(it.email), it.body) }
                    .toList()
}
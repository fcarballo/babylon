package com.babylon.fabiocarballo.posts.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.babylon.fabiocarballo.R
import com.babylon.fabiocarballo.extensions.toGone
import com.babylon.fabiocarballo.extensions.toVisible
import com.babylon.fabiocarballo.internals.BaseActivity
import com.babylon.fabiocarballo.posts.PostViewModel
import kotlinx.android.synthetic.main.activity_post_details.*
import kotlinx.android.synthetic.main.layout_error.*
import org.jetbrains.anko.intentFor
import org.parceler.Parcels
import javax.inject.Inject

class PostDetailsActivity : BaseActivity(), PostDetailsPresenter.View {

    @Inject
    lateinit var presenter: PostDetailsPresenter

    private val commentsAdapter by lazy { PostCommentsAdapter(this) }

    private val post: PostViewModel by lazy {
        Parcels.unwrap<PostViewModel>(intent.extras.getParcelable(POST))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_post_details)

        activityComponent.postDetailsActivityComponentBuilder()
                .plus(PostDetailsActivityModule(this, post.id))
                .build()
                .inject(this)

        supportActionBar?.let {
            it.title = getString(R.string.post_details)
            it.setDisplayHomeAsUpEnabled(true)
        }

        with(recyclerView) {
            layoutManager = android.support.v7.widget.LinearLayoutManager(this@PostDetailsActivity,
                    LinearLayoutManager.VERTICAL, false)
            addItemDecoration(DividerItemDecoration(recyclerView.context, LinearLayoutManager.VERTICAL))
            adapter = commentsAdapter
        }

        refreshLayout.setOnRefreshListener { presenter.start() }

        if (savedInstanceState == null) {
            post.let { displayPostBasicInfo(post) }
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }

    override fun setPostDetails(postDetails: PostDetailsViewModel) {
        displayPostBasicInfo(postDetails.post)

        numComments.toVisible()
        numComments.text = resources.getQuantityString(R.plurals.post_comments,
                postDetails.comments.size, postDetails.comments.size)

        commentsAdapter
                .also { it.commentsList = postDetails.comments }
                .also { it.notifyDataSetChanged() }

        recyclerView.toVisible()
        errorView.toGone()

        setLoadingVisibility(false)
    }

    private fun displayPostBasicInfo(post: PostViewModel) {
        postView.bind(post)
    }

    override fun showNoConnectionPlaceholder() {
        displayError(R.string.error_connection)
    }

    override fun showTimeoutPlaceholder() {
        displayError(R.string.error_timeout)
    }

    override fun showUnknownProblemPlaceholder() {
        displayError(R.string.error_unknown)
    }

    private fun displayError(@StringRes errorMsgResId: Int) {
        errorView.toVisible()
        errorMessage.text = getString(errorMsgResId)

        recyclerView.toGone()

        setLoadingVisibility(false)
    }

    private fun setLoadingVisibility(visible: Boolean) {
        if (!visible) {
            refreshLayout.isRefreshing = false
        }

        if (visible) {
            loadingProgress.toVisible()
        } else {
            loadingProgress.toGone()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }

        return super.onOptionsItemSelected(item)
    }

    companion object {

        private const val POST = "post"

        fun buildIntent(context: Context, post: PostViewModel): Intent {
            return context.intentFor<PostDetailsActivity>(POST to Parcels.wrap(post))
        }
    }
}
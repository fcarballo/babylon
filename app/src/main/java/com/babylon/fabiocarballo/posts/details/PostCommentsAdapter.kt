package com.babylon.fabiocarballo.posts.details

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.babylon.fabiocarballo.R
import com.babylon.fabiocarballo.extensions.loadUrl
import kotlinx.android.synthetic.main.elem_comment.view.*

class PostCommentsAdapter(private val context: Context) : RecyclerView.Adapter<PostCommentsAdapter.Holder>() {

    var commentsList: List<CommentViewModel> = listOf()

    private val inflater by lazy { LayoutInflater.from(context) }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = inflater.inflate(R.layout.elem_comment, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val view = holder.itemView
        val comment = commentsList[position]

        with(view) {
            username.text = comment.username
            commentBody.text = comment.body
            profilePicture.loadUrl(comment.userAvatarUrl, R.drawable.ic_person)
        }
    }

    override fun getItemCount() = commentsList.size

    class Holder(view: View) : RecyclerView.ViewHolder(view)
}
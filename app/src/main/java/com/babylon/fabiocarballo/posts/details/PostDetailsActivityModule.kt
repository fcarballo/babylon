package com.babylon.fabiocarballo.posts.details

import com.babylon.fabiocarballo.api.PlaceholderApi
import com.babylon.fabiocarballo.internals.Logger
import com.babylon.fabiocarballo.posts.AvatarGenerator
import dagger.Module
import dagger.Provides

@Module
class PostDetailsActivityModule(private val view: PostDetailsPresenter.View,
                                private val postId: Long) {

    @Provides
    fun providePostDetailsPresenter(getPostDetailsUseCase: GetPostDetailsUseCase,
                                    postDetailsModelMapper: PostDetailsModelMapper,
                                    logger: Logger): PostDetailsPresenter {
        return PostDetailsPresenter(view, postId, getPostDetailsUseCase, postDetailsModelMapper, logger)
    }

    @Provides
    fun provideGetPostDetailsUseCase(placeholderApi: PlaceholderApi,
                                     avatarGenerator: AvatarGenerator): GetPostDetailsUseCase {
        return GetPostDetailsUseCase(placeholderApi, avatarGenerator)
    }

    @Provides
    fun providePostDetailsModelMapper(): PostDetailsModelMapper {
        return PostDetailsModelMapper()
    }
}
package com.babylon.fabiocarballo.posts.details

import com.babylon.fabiocarballo.internals.Logger
import com.babylon.fabiocarballo.internals.exceptions.NoConnectionException
import com.babylon.fabiocarballo.internals.exceptions.TimeoutException
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription

class PostDetailsPresenter(
        private val view: View,
        private val postId: Long,
        private val getPostDetailsUseCase: GetPostDetailsUseCase,
        private val modelMapper: PostDetailsModelMapper,
        private val logger: Logger) {

    private val subscriptions = CompositeSubscription()

    fun start() {
        subscriptions.add(
                getPostDetailsUseCase.build(postId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    logger.debug("get post details!")

                                    view.setPostDetails(modelMapper.toViewModel(it))
                                },
                                {
                                    logger.error(it)

                                    when (it) {
                                        is NoConnectionException -> view.showNoConnectionPlaceholder()
                                        is TimeoutException -> view.showTimeoutPlaceholder()
                                        else -> view.showUnknownProblemPlaceholder()
                                    }
                                }
                        )
        )
    }

    fun stop() {
        subscriptions.clear()
    }

    interface View {
        fun setPostDetails(postDetailsPost: PostDetailsViewModel)
        fun showNoConnectionPlaceholder()
        fun showTimeoutPlaceholder()
        fun showUnknownProblemPlaceholder()
    }
}
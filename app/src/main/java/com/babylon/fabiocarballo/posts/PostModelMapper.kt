package com.babylon.fabiocarballo.posts

import com.babylon.fabiocarballo.domain.Post

class PostModelMapper {

    fun toViewModel(posts: List<Post>): List<PostViewModel> {
        return posts.map {
            PostViewModel(
                    id = it.id,
                    username = it.username,
                    userAvatarUrl = it.userAvatarUrl,
                    title = it.title,
                    body = it.body)
        }
    }
}
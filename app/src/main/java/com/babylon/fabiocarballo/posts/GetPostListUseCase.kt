package com.babylon.fabiocarballo.posts

import com.babylon.fabiocarballo.api.PlaceholderApi
import com.babylon.fabiocarballo.domain.Post
import com.babylon.fabiocarballo.domain.User
import com.babylon.fabiocarballo.api.ServerPostFormat
import rx.Observable
import rx.Single

class GetPostListUseCase(
        private val placeholderApi: PlaceholderApi,
        private val avatarGenerator: AvatarGenerator) {

    fun build(): Single<List<Post>> {
        return placeholderApi.getPosts()
                .flatMap { posts ->
                    val postsUserIds = posts.map { it.userId }.distinct()

                    getUsers(postsUserIds)
                            .map { users -> generatePosts(posts, users) }
                }
                .toSingle()
    }

    private fun getUsers(userIds: List<Long>): Observable<Map<Long, User>> {
        return placeholderApi.getUsers()
                .flatMap { Observable.from(it) }
                .filter { it.id in userIds }
                .map {
                    User(id = it.id, username = it.username, email = it.email,
                            avatarUrl = avatarGenerator.generateAvatarUrl(it.email))
                }
                .toList()
                .map { users -> users.associateBy { it.id } }
    }

    private fun generatePosts(posts: List<ServerPostFormat>,
                              users: Map<Long, User>): List<Post> {
        return posts.map {
            val user = users[it.userId]

            Post(id = it.id, userId = it.userId,
                    username = user?.username ?: "",
                    userAvatarUrl = user?.avatarUrl ?: "",
                    title = it.title, body = it.body)
        }
    }
}
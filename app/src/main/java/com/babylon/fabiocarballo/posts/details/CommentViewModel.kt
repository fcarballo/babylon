package com.babylon.fabiocarballo.posts.details

data class CommentViewModel(
        val username: String,
        val userAvatarUrl: String,
        val body: String)
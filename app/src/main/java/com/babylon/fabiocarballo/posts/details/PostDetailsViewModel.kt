package com.babylon.fabiocarballo.posts.details

import com.babylon.fabiocarballo.posts.PostViewModel

data class PostDetailsViewModel(
        val post: PostViewModel,
        val comments: List<CommentViewModel>)
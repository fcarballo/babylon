package com.babylon.fabiocarballo.posts

import com.babylon.fabiocarballo.Navigator
import com.babylon.fabiocarballo.api.PlaceholderApi
import com.babylon.fabiocarballo.internals.Logger
import dagger.Module
import dagger.Provides

@Module
class PostListActivityModule(
        private val view: PostListPresenter.View) {

    @Provides
    fun providesPostListPresenter(getPostListUseCase: GetPostListUseCase,
                                  logger: Logger,
                                  postModelMapper: PostModelMapper,
                                  navigator: Navigator): PostListPresenter {
        return PostListPresenter(view, getPostListUseCase, postModelMapper, navigator, logger)
    }

    @Provides
    fun provideGetPostListUseCase(placeholderApi: PlaceholderApi,
                                  avatarGenerator: AvatarGenerator): GetPostListUseCase {
        return GetPostListUseCase(placeholderApi, avatarGenerator)
    }

    @Provides
    fun providePostModelMapper(): PostModelMapper {
        return PostModelMapper()
    }
}
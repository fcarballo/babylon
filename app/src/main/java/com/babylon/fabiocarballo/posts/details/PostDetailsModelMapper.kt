package com.babylon.fabiocarballo.posts.details

import com.babylon.fabiocarballo.domain.DetailedPost
import com.babylon.fabiocarballo.domain.Post
import com.babylon.fabiocarballo.posts.PostViewModel

class PostDetailsModelMapper {

    fun toViewModel(post: DetailedPost): PostDetailsViewModel {
        return PostDetailsViewModel(
                post = PostViewModel(post.id, post.username, post.userAvatarUrl, post.title, post.body),
                comments = post.comments.map {
                    CommentViewModel(it.username, it.userAvatarUrl, it.body)
                })
    }
}
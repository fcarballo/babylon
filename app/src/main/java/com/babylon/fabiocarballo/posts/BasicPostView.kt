package com.babylon.fabiocarballo.posts

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.babylon.fabiocarballo.R
import com.babylon.fabiocarballo.extensions.loadUrl
import kotlinx.android.synthetic.main.elem_post_simple.view.*

class BasicPostView : LinearLayout {

    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init()
    }

    private fun init() {
        orientation = LinearLayout.VERTICAL

        View.inflate(context, R.layout.elem_post_simple, this)
    }

    var clickListener: ClickListener? = null

    fun bind(post: PostViewModel) {
        postTitle.text = post.title
        postBody.text = post.body

        username.text = post.username
        profilePicture.loadUrl(post.userAvatarUrl, R.drawable.ic_person)

        setOnClickListener { clickListener?.onPostClick(post) }
    }

    interface ClickListener {
        fun onPostClick(post: PostViewModel)
    }
}
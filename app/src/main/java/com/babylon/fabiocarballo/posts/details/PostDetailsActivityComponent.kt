package com.babylon.fabiocarballo.posts.details

import dagger.Subcomponent

@Subcomponent(modules = arrayOf(PostDetailsActivityModule::class))
interface PostDetailsActivityComponent {

    @Subcomponent.Builder
    interface Builder {
        fun plus(module: PostDetailsActivityModule): Builder
        fun build(): PostDetailsActivityComponent
    }

    fun inject(activity: PostDetailsActivity)
}
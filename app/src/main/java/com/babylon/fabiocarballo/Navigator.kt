package com.babylon.fabiocarballo

import android.content.Context
import com.babylon.fabiocarballo.posts.PostViewModel
import com.babylon.fabiocarballo.posts.details.PostDetailsActivity
import android.support.v4.view.ViewCompat
import android.support.v4.app.ActivityOptionsCompat



class Navigator(private val context: Context) {

    fun openPostDetails(postViewModel: PostViewModel) {
        context.startActivity(PostDetailsActivity.buildIntent(context, postViewModel))
    }
}
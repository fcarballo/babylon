package com.babylon.fabiocarballo.api

data class ServerUserFormat(
        val id: Long,
        val name: String ,
        val username: String,
        val email: String)
package com.babylon.fabiocarballo.api

import retrofit2.http.GET
import retrofit2.http.Path
import rx.Observable

interface PlaceholderApi {

    @GET("posts")
    fun getPosts(): Observable<List<ServerPostFormat>>

    @GET("users")
    fun getUsers(): Observable<List<ServerUserFormat>>

    @GET("posts/{id}")
    fun getPost(@Path("id") id: Long): Observable<ServerPostFormat>

    @GET("users/{id}")
    fun getUser(@Path("id") id: Long): Observable<ServerUserFormat>

    @GET("posts/{id}/comments")
    fun getComments(@Path("id") postId: Long): Observable<List<ServerCommentFormat>>

    companion object {
        const val BASE_URL = "http://jsonplaceholder.typicode.com/"
    }

}

package com.babylon.fabiocarballo.api

data class ServerCommentFormat(
        val id: Long,
        val name: String,
        val email: String,
        val body: String)
package com.babylon.fabiocarballo.api

data class ServerPostFormat(
        val userId: Long,
        val id: Long,
        val title: String = "",
        val body: String = "")
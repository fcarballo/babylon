package com.babylon.fabiocarballo.domain

data class Post(
        val id: Long,
        val userId: Long,
        val username: String,
        val userAvatarUrl: String,
        val title: String,
        val body: String)
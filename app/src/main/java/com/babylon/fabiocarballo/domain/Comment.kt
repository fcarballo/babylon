package com.babylon.fabiocarballo.domain

data class Comment(
        val id: Long,
        val username: String,
        val userAvatarUrl: String,
        val body: String)
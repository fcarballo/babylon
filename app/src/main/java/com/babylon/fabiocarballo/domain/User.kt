package com.babylon.fabiocarballo.domain

data class User(
        val id: Long,
        val username: String,
        val email: String,
        val avatarUrl: String)
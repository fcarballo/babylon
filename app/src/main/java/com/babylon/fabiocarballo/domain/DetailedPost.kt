package com.babylon.fabiocarballo.domain

data class DetailedPost(
        val id: Long,
        val userId: Long,
        val username: String,
        val userAvatarUrl: String,
        val title: String,
        val body: String,
        val comments: List<Comment>)
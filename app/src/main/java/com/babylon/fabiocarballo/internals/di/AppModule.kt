package com.babylon.fabiocarballo.internals.di

import com.babylon.fabiocarballo.internals.Logger
import com.babylon.fabiocarballo.posts.AvatarGenerator
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideLogger(): Logger {
        return Logger()
    }

    @Provides
    fun provideAvatarGenerator(): AvatarGenerator {
        return AvatarGenerator()
    }
}
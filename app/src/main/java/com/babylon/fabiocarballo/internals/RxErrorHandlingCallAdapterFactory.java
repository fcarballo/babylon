package com.babylon.fabiocarballo.internals;

import com.babylon.fabiocarballo.internals.exceptions.NoConnectionException;
import com.babylon.fabiocarballo.internals.exceptions.TimeoutException;

import java.io.InterruptedIOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import rx.Completable;
import rx.Observable;
import rx.Single;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * This class intercepts errors on RxJava requests and transforms some of the errors into
 * appropriate domain exceptions such as {@link NoConnectionException} or
 * {@link TimeoutException}
 * <p>
 * This way we can deal with domain exceptions in the presenters and use cases.
 */
public class RxErrorHandlingCallAdapterFactory extends CallAdapter.Factory {
    private final RxJavaCallAdapterFactory original;

    private RxErrorHandlingCallAdapterFactory() {
        original = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());
    }

    public static CallAdapter.Factory create() {
        return new RxErrorHandlingCallAdapterFactory();
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public CallAdapter get(Type returnType, Annotation[] annotations, Retrofit retrofit) {
        return new RxCallAdapterWrapper(retrofit, original.get(returnType, annotations, retrofit));
    }

    private static class RxCallAdapterWrapper implements CallAdapter {
        private final Retrofit retrofit;
        private final CallAdapter wrapped;

        public RxCallAdapterWrapper(Retrofit retrofit, CallAdapter wrapped) {
            this.retrofit = retrofit;
            this.wrapped = wrapped;
        }

        @Override
        public Type responseType() {
            return wrapped.responseType();
        }

        @SuppressWarnings({"unchecked", "NullableProblems"})
        @Override
        public Object adapt(Call call) {
            Object adaptedCall = wrapped.adapt(call);

            if (adaptedCall instanceof Completable) {
                return ((Completable) adaptedCall).onErrorResumeNext(new Func1<Throwable, Completable>() {
                    @Override
                    public Completable call(Throwable throwable) {
                        return Completable.error(transformedThrowable(throwable));
                    }
                });
            }

            if (adaptedCall instanceof Single) {
                return ((Single) adaptedCall).onErrorResumeNext(new Func1<Throwable, Single>() {
                    @Override
                    public Single call(Throwable throwable) {
                        return Single.error(transformedThrowable(throwable));
                    }
                });
            }

            if (adaptedCall instanceof Observable) {
                return ((Observable) adaptedCall).onErrorResumeNext(new Func1<Throwable, Observable>() {
                    @Override
                    public Observable call(Throwable throwable) {
                        return Observable.error(transformedThrowable(throwable));
                    }
                });
            }

            throw new RuntimeException("Observable Type not supported");
        }

        private Throwable transformedThrowable(Throwable throwable) {
            if (throwable instanceof SocketTimeoutException || throwable instanceof InterruptedIOException) {
                return new TimeoutException();
            }

            if (throwable instanceof UnknownHostException) {
                return new NoConnectionException();
            }

            return throwable;
        }
    }
}
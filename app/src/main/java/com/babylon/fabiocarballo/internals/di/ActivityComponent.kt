package com.babylon.fabiocarballo.internals.di

import com.babylon.fabiocarballo.posts.PostListActivityComponent
import com.babylon.fabiocarballo.posts.details.PostDetailsActivityComponent
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(ActivityModule::class))
@ActivityScope
interface ActivityComponent {

    @Subcomponent.Builder
    interface Builder {
        fun plus(module: ActivityModule): Builder
        fun build(): ActivityComponent
    }

    fun postListActivityComponentBuilder(): PostListActivityComponent.Builder

    fun postDetailsActivityComponentBuilder(): PostDetailsActivityComponent.Builder
}
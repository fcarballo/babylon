package com.babylon.fabiocarballo.internals.networking

import com.babylon.fabiocarballo.internals.RxErrorHandlingCallAdapterFactory
import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class RetrofitFactory {

    fun build(url: String, client: OkHttpClient = OkHttpClient()): Retrofit {
        return Retrofit.Builder()
                .baseUrl(url)
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .client(client)
                .build()
    }
}
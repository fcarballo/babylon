package com.babylon.fabiocarballo.internals.di

import android.app.Activity
import com.babylon.fabiocarballo.Navigator
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: Activity) {

    @Provides
    @ActivityScope
    fun provideActivity(): Activity {
        return activity
    }

    @Provides
    fun provideAppNavigator(): Navigator {
        return Navigator(activity)
    }
}
package com.babylon.fabiocarballo.internals

import timber.log.Timber

class Logger {

    fun debug(msg: String) {
        Timber.d(msg)
    }

    fun error(throwable: Throwable) {
        Timber.e(throwable)
    }
}
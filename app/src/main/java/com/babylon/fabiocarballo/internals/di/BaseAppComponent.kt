package com.babylon.fabiocarballo.internals.di


interface BaseAppComponent {

    fun activityComponentBuilder(): ActivityComponent.Builder
}

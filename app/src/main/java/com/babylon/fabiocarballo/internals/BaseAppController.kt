package com.babylon.fabiocarballo.internals

import android.app.Application
import com.babylon.fabiocarballo.internals.di.AppModule
import com.babylon.fabiocarballo.internals.di.BaseAppComponent
import com.babylon.fabiocarballo.internals.di.DaggerAppComponent

abstract class BaseAppController : Application() {

    lateinit var appComponent: BaseAppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule())
                .build()
    }
}
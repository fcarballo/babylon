package com.babylon.fabiocarballo.internals.exceptions

class NoConnectionException: RuntimeException()
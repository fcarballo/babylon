package com.babylon.fabiocarballo.posts

import com.babylon.fabiocarballo.api.PlaceholderApi
import com.babylon.fabiocarballo.api.ServerPostFormat
import com.babylon.fabiocarballo.api.ServerUserFormat
import com.babylon.fabiocarballo.domain.Post
import com.babylon.fabiocarballo.internals.exceptions.NoConnectionException
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Before
import org.junit.Test
import rx.Observable
import rx.observers.TestSubscriber

class GetPostListUseCaseTest {

    val avatarGenerator: AvatarGenerator = mock()
    val api: PlaceholderApi = mock()

    val tested: GetPostListUseCase = GetPostListUseCase(api, avatarGenerator)

    lateinit var testSubscriber: TestSubscriber<List<Post>>

    @Before
    fun setup() {
        testSubscriber = TestSubscriber()
    }

    @Test
    fun test_ApiRequestHasSuccess_ShouldReturnCorrectListOfPosts() {
        val serverPosts: List<ServerPostFormat> = listOf(
                ServerPostFormat(userId = 1, id = 10, title = "my-post-title", body = "my-post-body"),
                ServerPostFormat(userId = 1, id = 11, title = "my-post", body = "some text"),
                ServerPostFormat(userId = 2, id = 20, title = "my-title", body = "more text"))

        val serverUsers: List<ServerUserFormat> = listOf(
                ServerUserFormat(1, "fabio carballo", "fabiocarballo", "fabiocarballo@mail.com"),
                ServerUserFormat(2, "the other", "theotherone", "theotherone@mail.com"))

        api.let {
            whenever(it.getPosts()).thenReturn(Observable.just(serverPosts))
            whenever(it.getUsers()).thenReturn(Observable.just(serverUsers))
        }

        avatarGenerator.let {
            whenever(it.generateAvatarUrl("fabiocarballo@mail.com")).thenReturn("avatar1.png")
            whenever(it.generateAvatarUrl("theotherone@mail.com")).thenReturn("avatar2.png")
        }


        getPosts()

        testSubscriber.assertCompleted()
        testSubscriber.assertValue(
                listOf(
                        Post(10, 1, "fabiocarballo", "avatar1.png", "my-post-title", "my-post-body"),
                        Post(11, 1, "fabiocarballo", "avatar1.png", "my-post", "some text"),
                        Post(20, 2, "theotherone", "avatar2.png", "my-title", "more text")))
    }

    @Test
    fun test_ApiRequestHasException_ShouldReturnThrownException() {
        val exception = NoConnectionException()

        whenever(api.getPosts()).thenReturn(Observable.error(exception))

        getPosts()

        testSubscriber.assertNoValues()
        testSubscriber.assertError(exception)
    }

    private fun getPosts() {
        tested.build().subscribe(testSubscriber)
    }
}
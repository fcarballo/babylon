package com.babylon.fabiocarballo.posts

import com.babylon.fabiocarballo.Navigator
import com.babylon.fabiocarballo.RxSchedulersOverrideRule
import com.babylon.fabiocarballo.domain.Post
import com.babylon.fabiocarballo.internals.Logger
import com.babylon.fabiocarballo.internals.exceptions.NoConnectionException
import com.babylon.fabiocarballo.internals.exceptions.TimeoutException
import com.nhaarman.mockito_kotlin.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import rx.Single

class PostListPresenterTest {

    val view: PostListPresenter.View = mock()
    val getPostListUseCase: GetPostListUseCase = mock()
    val logger: Logger = mock()
    val navigator: Navigator = mock()

    val tested = PostListPresenter(
            view,
            getPostListUseCase,
            PostModelMapper(),
            navigator,
            logger)

    @get:Rule
    val rule = RxSchedulersOverrideRule()

    @Before
    fun setup() {
        reset(view)
        reset(getPostListUseCase)
        reset(navigator)
    }

    @After
    fun tearDown() {
        tested.stop()
    }

    @Test
    fun start_SuccessfulRequest_ShouldDisplayPosts() {
        val posts = listOf(
                Post(1L, 10L, "fabiocarballo", "fabio.png", "this is a title", "this is a body"),
                Post(2L, 11L, "richard", "richard.png", "a small title", "a body"),
                Post(3L, 11L, "richard", "richard.png", "a random title", "a non empty body"),
                Post(4L, 12L, "johnny", "johnny.png", "a special title", "the content"))

        whenever(getPostListUseCase.build()).thenReturn(Single.just(posts))

        tested.start()

        val expectedPosts =
                listOf(
                        PostViewModel(1L, "fabiocarballo", "fabio.png", "this is a title", "this is a body"),
                        PostViewModel(2L, "richard", "richard.png", "a small title", "a body"),
                        PostViewModel(3L, "richard", "richard.png", "a random title", "a non empty body"),
                        PostViewModel(4L, "johnny", "johnny.png", "a special title", "the content"))

        verify(view).setPosts(expectedPosts)
    }

    @Test
    fun start_SuccesfulRequestWithEmptyList_ShouldShowEmptyPlaceholder() {
        val posts = listOf<Post>()

        whenever(getPostListUseCase.build()).thenReturn(Single.just(posts))

        tested.start()

        verify(view, never()).setPosts(any())
        verify(view).showEmptyPlaceholder()
    }

    @Test
    fun start_FailedRequestWithNoConnection_ShouldShowNoConnectionError() {
        whenever(getPostListUseCase.build()).thenReturn(Single.error(NoConnectionException()))

        tested.start()

        verify(view, never()).setPosts(any())
        verify(view).showNoConnectionPlaceholder()
    }

    @Test
    fun start_FailedRequestWithTimeout_ShouldShowTimeoutError() {
        whenever(getPostListUseCase.build()).thenReturn(Single.error(TimeoutException()))

        tested.start()

        verify(view, never()).setPosts(any())
        verify(view).showTimeoutPlaceholder()
    }

    @Test
    fun start_FailedRequestWithUnknownException_ShouldShowUnknownError() {
        whenever(getPostListUseCase.build()).thenReturn(Single.error(Exception("Weird exception")))

        tested.start()

        verify(view, never()).setPosts(any())
        verify(view).showUnknownProblemPlaceholder()
    }

    @Test
    fun onPostClick_ShouldOpenPostDetailScreen() {
        val post = PostViewModel(1L, "fabiocarballo", "fabio.png", "this is a title", "this is a body")

        tested.onPostClick(post)

        verify(navigator).openPostDetails(post)
    }
}
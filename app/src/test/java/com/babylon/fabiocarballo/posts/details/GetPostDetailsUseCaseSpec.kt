package com.babylon.fabiocarballo.posts.details

import com.babylon.fabiocarballo.api.ServerCommentFormat
import com.babylon.fabiocarballo.api.PlaceholderApi
import com.babylon.fabiocarballo.api.ServerPostFormat
import com.babylon.fabiocarballo.api.ServerUserFormat
import com.babylon.fabiocarballo.domain.Comment
import com.babylon.fabiocarballo.domain.DetailedPost
import com.babylon.fabiocarballo.internals.exceptions.NoConnectionException
import com.babylon.fabiocarballo.posts.AvatarGenerator
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.reset
import com.nhaarman.mockito_kotlin.whenever
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.*
import rx.Observable
import rx.observers.TestSubscriber

class GetPostDetailsUseCaseSpec : Spek({

    given("GetPostDetailsUseCase") {

        val api: PlaceholderApi = mock()
        val avatarGenerator: AvatarGenerator = mock()

        val tested = GetPostDetailsUseCase(api, avatarGenerator)

        var testSubscriber: TestSubscriber<DetailedPost> = TestSubscriber()

        beforeEachTest {
            testSubscriber = TestSubscriber()

            reset(api)
            reset(avatarGenerator)
        }

        describe("#build") {

            context("successful request") {
                val serverPost = ServerPostFormat(userId = 2L, id = 1L, title = "title", body = "body")

                val serverUser = ServerUserFormat(id = 2L, username = "fabiocarballo",
                        name = "Fábio Carballo", email = "fabiocarballo@mail.com")

                val serverComments = listOf(
                        ServerCommentFormat(id = 1L, name = "richard", email = "richard@mail.com", body = "Lame."),
                        ServerCommentFormat(id = 2L, name = "fabiocarballo", email = "fabiocarballo@mail.com", body = "You are always complaining."),
                        ServerCommentFormat(id = 3L, name = "richard", email = "richard@mail.com", body = "I\'m not."))

                beforeEachTest {
                    avatarGenerator.let {
                        whenever(it.generateAvatarUrl("fabiocarballo@mail.com")).thenReturn("fabio.png")
                        whenever(it.generateAvatarUrl("richard@mail.com")).thenReturn("richard_avatar.png")
                    }

                    api.let {
                        whenever(it.getPost(1L)).thenReturn(Observable.just(serverPost))
                        whenever(it.getUser(2L)).thenReturn(Observable.just(serverUser))
                        whenever(it.getComments(1L)).thenReturn(Observable.just(serverComments))
                    }

                    tested.build(1L).subscribe(testSubscriber)
                }

                it("should return a detailed post with the correct information") {
                    val expectedPost = DetailedPost(
                            id = 1L,
                            userId = 2L,
                            username = "fabiocarballo",
                            userAvatarUrl = "fabio.png",
                            title = "title",
                            body = "body",
                            comments = listOf(
                                    Comment(1L, "richard", "richard_avatar.png", "Lame."),
                                    Comment(2L, "fabiocarballo", "fabio.png", "You are always complaining."),
                                    Comment(3L, "richard", "richard_avatar.png", "I'm not.")))

                    testSubscriber.let {
                        it.assertNoErrors()
                        it.assertValue(expectedPost)
                        it.assertCompleted()
                    }
                }
            }

            context("failed request") {
                val exception = NoConnectionException()

                beforeEachTest {
                    whenever(api.getPost(1L)).thenReturn(Observable.error(exception))

                    tested.build(1L).subscribe(testSubscriber)
                }

                it("should return the thrown exception") {
                    testSubscriber.let {
                        it.assertNoValues()
                        it.assertError(exception)
                        it.assertNotCompleted()
                    }
                }
            }
        }
    }
})
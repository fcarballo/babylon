package com.babylon.fabiocarballo.posts.details

import com.babylon.fabiocarballo.domain.Comment
import com.babylon.fabiocarballo.domain.DetailedPost
import com.babylon.fabiocarballo.internals.Logger
import com.babylon.fabiocarballo.internals.exceptions.NoConnectionException
import com.babylon.fabiocarballo.internals.exceptions.TimeoutException
import com.babylon.fabiocarballo.posts.PostViewModel
import com.babylon.fabiocarballo.rxGroup
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.reset
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.*
import rx.subjects.PublishSubject

class PostDetailsPresenterSpec : Spek({

    rxGroup("PostDetailsPresenterSpec") {

        val postId = 1L
        val view: PostDetailsPresenter.View = mock()
        val getPostDetailsUseCase: GetPostDetailsUseCase = mock()
        val logger: Logger = mock()

        val tested = PostDetailsPresenter(view, postId, getPostDetailsUseCase, PostDetailsModelMapper(), logger)

        var getPostDetailsPubSub: PublishSubject<DetailedPost> = PublishSubject.create()

        beforeEachTest {
            getPostDetailsPubSub = PublishSubject.create()

            reset(view)
            reset(getPostDetailsUseCase)

            whenever(getPostDetailsUseCase.build(postId)).thenReturn(getPostDetailsPubSub.toSingle())
        }

        describe("start flow") {
            beforeEachTest {
                tested.start()
            }

            afterEachTest {
                tested.stop()
            }

            it("should request the post details") {
                verify(getPostDetailsUseCase).build(postId)
            }

            given("a successful request") {
                beforeEachTest {
                    val post = DetailedPost(
                            id = 1L,
                            userId = 10L,
                            username = "fabio",
                            userAvatarUrl = "fabio.png",
                            title = "title",
                            body = "body",
                            comments = listOf(Comment(1L, "richard", "richard.png", "a comment")))

                    with(getPostDetailsPubSub) {
                        onNext(post)
                        onCompleted()
                    }
                }

                it("should set the post details") {
                    val expectedPost = PostDetailsViewModel(
                            PostViewModel(1, "fabio", "fabio.png", "title", "body"),
                            listOf(CommentViewModel("richard", "richard.png", "a comment")))

                    verify(view).setPostDetails(expectedPost)
                }
            }


            given("a failed request") {

                fun SpecBody.describeErrorScenario(throwable: Throwable,
                                                   description: String,
                                                   assertion: ((PostDetailsPresenter.View) -> Unit)) {

                    context("error is ${throwable.javaClass.simpleName}") {
                        beforeEachTest {
                            getPostDetailsPubSub.onError(throwable)
                        }

                        it(description) {
                            assertion(view)
                        }
                    }
                }

                describeErrorScenario(NoConnectionException(), "should set the no connection placeholder") {
                    verify(it).showNoConnectionPlaceholder()
                }

                describeErrorScenario(TimeoutException(), "should set the timeout placeholder") {
                    verify(it).showTimeoutPlaceholder()
                }

                describeErrorScenario(Exception("Weird Exception"), "should set the unknown problem placeholder") {
                    verify(it).showUnknownProblemPlaceholder()
                }
            }
        }
    }
})
package com.babylon.fabiocarballo

import org.jetbrains.spek.api.dsl.SpecBody
import rx.Scheduler
import rx.android.plugins.RxAndroidPlugins
import rx.android.plugins.RxAndroidSchedulersHook
import rx.plugins.RxJavaHooks
import rx.schedulers.Schedulers


fun SpecBody.rxGroup(description: String, body: SpecBody.() -> Unit) {

    beforeEachTest {
        RxAndroidPlugins.getInstance().reset()
        RxAndroidPlugins.getInstance().registerSchedulersHook(object : RxAndroidSchedulersHook() {
            override fun getMainThreadScheduler(): Scheduler {
                return Schedulers.immediate()
            }
        })

        RxJavaHooks.setOnIOScheduler { rx.schedulers.Schedulers.immediate() }
        RxJavaHooks.setOnNewThreadScheduler { rx.schedulers.Schedulers.immediate() }
    }

    group("group $description", body = body)


    afterEachTest {
        RxAndroidPlugins.getInstance().reset()
        RxJavaHooks.clear()
    }
}

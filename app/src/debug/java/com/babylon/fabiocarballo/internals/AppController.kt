package com.babylon.fabiocarballo.internals

import com.facebook.stetho.Stetho
import timber.log.Timber

class AppController : BaseAppController() {

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)

        Timber.plant(Timber.DebugTree())
    }
}
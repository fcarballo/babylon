package com.babylon.fabiocarballo.posts

import android.support.test.InstrumentationRegistry
import android.support.test.rule.ActivityTestRule
import com.babylon.fabiocarballo.RxSchedulersOverrideRule
import com.babylon.fabiocarballo.api.PlaceholderApi
import com.babylon.fabiocarballo.api.ServerPostFormat
import com.babylon.fabiocarballo.api.ServerUserFormat
import com.babylon.fabiocarballo.internals.BaseAppController
import com.babylon.fabiocarballo.internals.di.AppComponent
import com.babylon.fabiocarballo.internals.di.NetworkModule
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import it.cosenonjaviste.daggermock.DaggerMock
import org.junit.Rule
import org.junit.Test
import rx.Observable

class PostsListActivityTest {

    @get:Rule
    val daggerRule = DaggerMock.rule<AppComponent>(NetworkModule()) {
        set {
            val app = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as BaseAppController
            app.appComponent = it
        }
    }

    @get:Rule
    var activityRule = ActivityTestRule(PostListActivity::class.java, false, false)

    @get:Rule
    val rule = RxSchedulersOverrideRule()

    val api: PlaceholderApi = mock()

    @Test
    fun data_is_displayed() {
        val serverPosts: List<ServerPostFormat> = listOf(
                ServerPostFormat(userId = 1, id = 10, title = "my-post-title", body = "my-post-body"),
                ServerPostFormat(userId = 1, id = 11, title = "my-post", body = "some text"),
                ServerPostFormat(userId = 2, id = 20, title = "my-title", body = "more text"))

        val serverUsers: List<ServerUserFormat> = listOf(
                ServerUserFormat(1, "fabio carballo", "fabiocarballo", "fabiocarballo@mail.com"),
                ServerUserFormat(2, "the other", "theotherone", "theotherone@mail.com"))


        whenever(api.getPosts()).thenReturn(Observable.just(serverPosts))
        whenever(api.getUsers()).thenReturn(Observable.just(serverUsers))

        activityRule.launchActivity(null)

        PostsListTestingRobot.assert {
            hasSize(3)

            hasItem(0, "fabiocarballo", "my-post-title", "my-post-body")
            hasItem(1, "fabiocarballo", "my-post", "some text")
            hasItem(2, "theotherone", "my-title", "more text")
        }
    }
}
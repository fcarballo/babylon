package com.babylon.fabiocarballo.posts

import android.support.test.espresso.Espresso
import android.support.test.espresso.assertion.ViewAssertions.*
import android.support.test.espresso.matcher.ViewMatchers.*
import com.babylon.fabiocarballo.R
import com.babylon.fabiocarballo.RecyclerViewItemCountAssertion
import com.babylon.fabiocarballo.RecyclerViewMatcher

class PostsListTestingRobot {

    companion object {
        fun assert(func: PostsListTestingRobot.() -> Unit) = PostsListTestingRobot().apply(func)
    }

    fun hasSize(size: Int) {
        Espresso.onView(withId(R.id.recyclerView))
                .check(RecyclerViewItemCountAssertion(size))
    }

    fun hasItem(position: Int, name: String, title: String, body: String) {
        Espresso.onView(RecyclerViewMatcher(R.id.recyclerView).atPosition(position))
                .check(matches(hasDescendant(withText(title))))
                .check(matches(hasDescendant(withText(body))))
                .check(matches(hasDescendant(withText(name))))
    }
}
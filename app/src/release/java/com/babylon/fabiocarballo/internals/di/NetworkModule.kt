package com.babylon.fabiocarballo.internals.di

import com.babylon.fabiocarballo.api.PlaceholderApi
import com.babylon.fabiocarballo.internals.networking.RetrofitFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun providePlaceholderApi(retrofitFactory: RetrofitFactory,
                              httpClient: OkHttpClient): PlaceholderApi {
        return retrofitFactory.build(PlaceholderApi.Companion.BASE_URL, httpClient)
                .run { create(PlaceholderApi::class.java) }
    }

    @Provides
    fun provideClient(): OkHttpClient {
        return OkHttpClient.Builder().build()
    }

    @Provides
    fun provideRetrofitFactory(): RetrofitFactory {
        return RetrofitFactory()
    }
}